var assetsObject = {
"audio": {
	"bgmusic": ["bgmusic.mp3", "bgmusic.ogg"],
},
"images": [
	"arrows.png",
	"arrowsBig.png",
	"backgroundGrass.png",
	"breathBarBg.png",
	"coins.png",
	"exitbutton.png",
	"favoicon.png",
	"highlight.png",
	"music.png",
	"nomusic.png",
	"nosound.png",
	"petitfours.png",
	"petitfoursiso.png",
	"petitfourslayers.png",
	"petitfoursTinyTray.png",
	"plate.png",
	"plate2.png",
	"sound.png"],
};