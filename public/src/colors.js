var pink = '#E22C8C';
var pinkLight = '#FDCBE6';
var green = '#AAED16';
var greenMedium = "#dCeEbC";
var greenLight = '#ECFECC';
var greenDark = '#005537';
var blue = '#26C5AA';
var blueLight = '#60E7D0';
var orange =  '#FF8532'; 
var orangeLight =  '#FFB18B'; 


mycolors = {
    pink: pink,
    pinkLight : pinkLight,
    green: green,
    greenLight : greenLight,
    greenDark : greenDark,
    blue: blue,
    blueLight: blueLight,
    orange: orange,
    orangeLight : orangeLight,

    background : '#ffffff',
    neutral: '#eeeeee',
    neutralhover: '#F2FABB',
    tiletaken: '#CCD7DB',
    player1: '#0000ff',
    player2: '#ff0000', 
    stoneborder: '#cccccc',

    rulesBg : '#eeeeee',
    rulesText : '#000000',
    rulesPageCounter : '#000000',
    countdownText: '#eeffee',
   
    title: pink,
    titleshadow: pinkLight,

    buttontext: '#ffffff',
    buttontextshadow: '#003300',
    buttontexthover: '#ffcccc',
    buttontexthovershadow: '#FFFFFF',

    button1: pinkLight, //'#B4F453', 
    button1Highlight: pink, //'#D7FF99', 
    gameBox: greenMedium, //'#FFA599',
    gameBoxBorder: green,//pink,
    gameLineBg: pinkLight,
    gameLineBgSelect: orange,
    gameLineBgHover: orangeLight,
    gameLineColor: '#000000',


    chalk : '#eeeedd',
}