var mutemusic = true;//false;
// console.log('music muted');
var mutesound = false;
var debug = false;//true;
var playing = false;
var fontFamily1 = "Monoton";//'Baloo Paaji', cursive; //"Josefin Sans";//"'Josefin Sans', sans-serif"; 
var fontFamily2 = "Amatic SC"; //Parisienne";
var fontFamily3 = "Parisienne";
var mouseButtons = {left: 0, middle: 1, right: 2};

// # Choices per layers of cake
	nrOfChoices = 5;

function saveState(state) {
	window.localStorage.setItem("gameState", JSON.stringify(state));
}
		 
function restoreState() {
	var state = window.localStorage.getItem("gameState");
	if (state) {
		return JSON.parse(state);
	} else {
		return null;
	}
}

if(restoreState()){
	
	var dummyvar = restoreState();
	if(dummyvar['highscore']){
		highscore = dummyvar['highscore'];
	}
}



Crafty.paths({
	audio: 'assets/sound/',
	images: 'assets/images/'
});

Game = {
	width: function() {
		return 800;
	},

	height: function() {
		return 600;
	},

    
    
	start: function() {
		
		
		// Start crafty and set a background color so that we can see it's working
		Crafty.init(Game.width(),Game.height(), 'cr-stage');
		Crafty.canvasLayer.init();
		Crafty.viewport.clampToEntities = false;
    //Crafty.viewport.zoom(20,0,0,100);
		ctx = Crafty.canvasLayer.context;
    Crafty.pixelart(true);
		Crafty.scene('Loading');
	},
	

};