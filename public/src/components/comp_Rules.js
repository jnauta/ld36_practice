Crafty.c("Rules", {
	init: function() {
		this.requires("2D, DOM,Text, Mouse, Tween");
		this.attr({x:97, y:77, w:560, h:410, alpha:1, z:1100}) ;
		this.textFont({size: '30px',  family: fontFamily3});
		this.css({'border-radius':' 1px','cursor': 'default' ,'background-color':mycolors.rulesBg, 'color':mycolors.rulesText, 'border':'3px solid #AAED16' ,'padding':'20px'});
		


		this.buttonRight = Crafty.e('2D, DOM,  arrowBig, Mouse').attr({x:570,y:470,h:60,w:60,z:1110})
		.css({'cursor':'pointer'})
		.bind('MouseOver',function(){this.sprite(1,0)})
		.bind('MouseOut',function(){this.sprite(0,0)})
		.bind('Click',function(){
            Crafty.trigger('nextPage');
		});
		
	
		
		this.buttonLeft = Crafty.e('2D, DOM,   arrowBig, Mouse').attr({x:170,y:470,h:60,w:60,z:1110})
		.css({'cursor':'pointer'}).flip("X")
		.bind('MouseOver',function(){this.sprite(1,0)})
		.bind('MouseOut',function(){this.sprite(0,0)})
		.bind('Click',function(){
            Crafty.trigger('previousPage');
		});
		
		this.rulesPageCounter = Crafty.e('2D, DOM,  Text,').attr({x:300,y:470,h:40,w:200,z:1110})
		.css({'text-align':'center', 'color':mycolors.rulesPageCounter,})
		.textFont({size: '42px',  family: fontFamily3});
		

		this.buttonExitRules = Crafty.e('2D, DOM, exitButton, Mouse').attr({x:681,y:58,h:40,w:40,z:1110})
		.css({'text-align':'center', 'cursor':'pointer'})
		.bind('Click',function(){Crafty.trigger('exitRulesPage')});

		this.page0 = "This game basically is connect-four in which the players simultaneously take a turn.";
		this.page1 = "When you both click the same tile, something has to be sorted out."; 
		this.page2 = "Some more text. <br><br> Mooorreeee."; 
		this.pages = [this.page0, this.page1, this.page2];
		this.currentPage = 0;
		this.text(this.pages[this.currentPage]);
		// this.unbind('nextPage');
		
		this.rulesPageCounter.text( (this.currentPage + 1) + '/' + this.pages.length);


		

		
	},
	
	_Rules : function(){
		
		
		
		this.bind('nextPage', function(){this.nextPage(1)});
		this.bind('previousPage', function(){this.nextPage(-1)});
		this.bind('exitRulesPage',this.exitRulesPage);

		this.attach(this.buttonRight);
		this.attach(this.buttonLeft);
		this.attach(this.rulesPageCounter);
		this.attach(this.buttonExitRules);

		return this;
	},

	nextPage : function(direction){
		// if(this.currentPage === this.pages.length - 1) {
  //       	this.visible = false;
  //       	this.destroy();
        // } else {
    	this.currentPage += direction + this.pages.length;	
    	this.currentPage = this.currentPage % this.pages.length;
    	this.rulesPageCounter.text((this.currentPage + 1) + '/' + this.pages.length );

    	this.text(this.pages[this.currentPage]);	
	},

	exitRulesPage : function(){
		this.visible = false;
        	this.destroy();
	},
	
});
