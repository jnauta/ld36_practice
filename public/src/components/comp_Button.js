Crafty.c("Button",{
	init: function(){
		this.requires( "2D, DOM, arrow, Mouse");
		this.bind('MouseOver',function(){this.sprite(1,0)});
		this.bind('MouseOut',function(){this.sprite(0,0)});
		this.css({'cursor': 'pointer'});
		this.attr({h:20, w:15, z:50});

	},

	_Button : function(layerIndex, diff){
		this.bind('MouseUp', function(){ 
			changeMyCake( layerIndex, diff, nrOfChoices);
		});

		return this;
	}

	
})

