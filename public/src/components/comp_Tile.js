Crafty.c("Tile", {
  init: function() {

    this.requires("2D, DOM, Mouse, Delay,  ");
    this.z = 1000;
    this.h = tileh;
    this.w = tilew;

    this.areaMap(new Crafty.polygon([35, 0, 70, 25, 35, 50, 0, 25]));

    // this.color(mycolors.neutral);
    this.attr({
      alpha: 0
    });
    this.wanted = false;
    this.countdown = 0; // used for tie pie.

    this.highlightPic = Crafty.e("2D, Canvas, Tween, Delay, highlight").attr({
      h: 50,
      w: 70,
      alpha: 0,
      z: 0
    }).origin('center');

    this.petitfourBottom = Crafty.e("2D, Canvas, Tween, Delay, bottomlayerSprite").attr({
      h: 50,
      w: 70,
      alpha: 0,
      z: 1
    }).origin('center');
    this.petitfourMiddle = Crafty.e("2D, Canvas, Tween, Delay, middlelayerSprite").attr({
      h: 50,
      w: 70,
      alpha: 0,
      z: 2
    }).origin('center');
    this.petitfourTopping = Crafty.e("2D, Canvas, Tween, Delay, toppingSprite").attr({
      h: 50,
      w: 70,
      alpha: 0,
      z: 3
    }).origin('center');
    this.petitfour = [this.petitfourBottom, this.petitfourMiddle, this.petitfourTopping]; //Crafty.e("2D, DOM, Tween, petitfour1").attr({alpha: 0});

    
  },

  _Tile: function(xpos, ypos, type, cakeStyles) {
    this.xpos = xpos;
    this.ypos = ypos;
    this.cakeStyles = cakeStyles;
    this.setCakeStyle(cakeStyles[type - 1]);
    // this.x = startTileX + tilew*this.xpos; 
    // this.y = startTileY + tileh*this.ypos;
    //this.petitfour.attr({x: this.x, y: this.y});
    iso.place(this.petitfour[0], this.xpos, this.ypos, 0);
    iso.place(this.petitfour[1], this.xpos, this.ypos, 0);
    iso.place(this.petitfour[2], this.xpos, this.ypos, 0);
    iso.place(this.highlightPic, this.xpos, this.ypos, 0);

    // each player has her own stoneType
    this.stoneType = 0;
    // this.color(mycolors.neutral); 
    // this.style({border = 1px bold black});    
    this.css({
      'text-align': 'center',
      'color': '#777777',
    });

    this.bind('MouseUp', function(e) {
      if (e.mouseButton === Crafty.mouseButtons.LEFT && this.stoneType === 0 && turnStatus !== "waitForDisplace" && !gameOver) {
        console.log("GameID: " + joinedGameID);
        socket.emit("player move", {
          playerID: playerID,
          gameID: joinedGameID,
          xpos: this.xpos,
          ypos: this.ypos
        });
        changeWantedMove(this.xpos, this.ypos);
      };


    });

    this.bind('MouseOver', function(e) {

      if (this.stoneType === 0 && turnStatus !== "waitForDisplace" && !gameOver) {
        // this.startBlink();
        if (turnStatus !== "displace" ||   (Math.abs(this.xpos - displacePos.x) <= 1 && Math.abs(this.ypos - displacePos.y) <= 1)) {
          this.changeAlpha(0.5);
        }
        // console.log('stoneType: ' + playerStoneType);

        // this.css({'cursor': 'pointer'});  

      }
    });

    this.bind('MouseOut', function(e) {
      if (this.stoneType === 0) {
        if (!this.wanted) {
          this.changeAlpha(0);
        }
      }
    });

    return this;
  },

  // pass in a 2D cakeStyle array
  setCakeStyle: function(cakeStyle) {
    this.petitfour[0].sprite(cakeStyle[0], 0);
    this.petitfour[1].sprite(cakeStyle[1], 1);
    this.petitfour[2].sprite(cakeStyle[2], 2);
  },

  setStoneType: function(stoneType) {
    this.stoneType = stoneType;
    if (stoneType !== 0) {
      if (stoneType >= 3) {
        this.countdown = stoneType - 1;
        stoneType = 3;
        this.countdownDrawing = this.attachCountdown(this.countdown);
      }
      if (this.countdown === 0 && this.countdownDrawing) {
        this.countdownDrawing.destroy();
      }
      this.setCakeStyle(this.cakeStyles[stoneType - 1]);
    }
  },

  startBlink: function() {
    if (turnStatus !== "waitForDisplace" && (turnStatus !== "displace" || (Math.abs(this.xpos - displacePos.x) <= 1 && Math.abs(this.ypos - displacePos.y) <= 1))) {
      this.blinkTimer = 0;
      this.bind('EnterFrame', this.blink);
      this.changeAlpha(1);
      this.wanted = true;
    }
  },

  blink: function() {
    this.blinkTimer += 1;
    if(this.blinkTimer === 50){
      this.changeAlpha(0.5);      
    }
    else if(this.blinkTimer >= 80){
      this.changeAlpha(1);
      this.blinkTimer = 0;

    }

  },

  stopBlink: function() {
    this.unbind('EnterFrame', this.blink);
    this.blinkTimer = 0;
    if (this.stoneType === 0) {
      this.changeAlpha(0);
    } else {
      this.changeAlpha(1);
    }
    this.wanted = false;
  },

  changeAlpha: function(alpha) {
    this.petitfour[0].attr({
      alpha: alpha
    })
    this.petitfour[1].attr({
      alpha: alpha
    })
    this.petitfour[2].attr({
      alpha: alpha
    })
  },

  attachCountdown: function(countdown) {
    this.countdownText = Crafty.e('2D, DOM, Text, Mouse').attr({
        x: this.x,
        y: this.y+10,
        w: 70,
        h: 40,
        z: 108
      })
      .text(this.countdown).textFont({
        size: '23px',
        type: 'italic',
        family: fontFamily2,
      })
      .css({
        'text-align': 'center',        
        'color': mycolors.countdownText,
      });
      this.petitfour[0].attach(this.countdownText);
  },

  decreaseCountdown: function() {
    if(this.countdown > 0) {
      --this.countdown;
      this.countdownText.text(this.countdown);
      if (this.countdown === 0) {
        this.stoneType = 0;
        this.changeAlpha(0);
        this.countdownText.destroy();
      }
    }
  },

  budge : function(source){
    
    var direction = [source[0]-this.xpos, source[1] - this.ypos];
    console.log('direction: ' + direction);
    var originalX = this._x;
    var originalY = this._y;
    var budgeStrength = 2;
    var tweenTime1 = 200;
    var tweenTime2 = 500;
    // this.petitfour[0].tween({x : originalX + direction[0]*budgeStrength, y : originalY + direction[1]*budgeStrength},tweenTime1, easeOutBounce);
    for(var i = 0 ; i < 3 ; i ++){
      this.petitfour[i].tween({y : originalY -( 10 - (Math.max(Math.abs(direction[0]), Math.abs(direction[1]))))*budgeStrength},tweenTime1, "easeOutQuad");
    
      this.petitfour[i].delay(function(){      
        this.tween({y:originalY}, tweenTime2, easeOutBounce);
      }, tweenTime1, 0);
    }
    

  
  },
})