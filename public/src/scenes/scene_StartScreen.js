Crafty.scene('StartScreen', function() {
	//Crafty.background('url(assets/images/backgroundGrass.png)');
	//Crafty.background(mycolors.background);
	
	// console.log('Startscreen');

	Crafty.timer.FPS(80);
   
    // background = Crafty.e('2D,DOM,Image').image('assets/images/background.png').attr({x:0, y:0, w:400,h:320, z:0});
    Crafty.background(mycolors.background);
	// Crafty.background('url(assets/images/background.png)');
    
    // Title
	Crafty.e('2D, DOM, Text, Mouse').attr({     x: 200,  y:0 ,   w:400, h:100 ,z:108  })
    .text('Petit &nbsp Fours').textFont({ size: '53px',type: 'italic', family: fontFamily1})
		.css({'padding-top': '5px','text-align':'center', 'text-shadow':'2px 2px '+ mycolors.titleshadow, 'color':mycolors.title});

   // name input box + random name maker ;)
  var names0 = ["Lovely ", "Sweet ", "Salty ", "Blue ","Fresh ", "Heavenly ","Delicious ", "Sparkly ", "Spicy ", "Soft ", "Creamy ", "Juicy ", "Rich ", "Sticky ", "Luscious ", "Tasty ", "Sensual " ] ;
  var names1 = ["Strawberry ", "Chocolate ", "Apple ",  "Minty ", "Soft ", "Banana ", "Jelly ", "Rose ", "Nuts ", "Caramel ", "Lemon ", "Cherry ", "Coconut "];
  var names2 = ["Pie","Cake", "Gateau", "Snack", "Dream", "Tart", "Pudding", "Roll", "Strudel"];
  var rand0 = Math.floor(Math.random()*names0.length);
  var rand1 = Math.floor(Math.random()*names1.length);
  var rand2 = Math.floor(Math.random()*names2.length);
  randomName = names0[rand0]+names1[rand1]+names2[rand2];
  
  nameInputBox = Crafty.e("HTML")
   .attr({x:274, y:250, w:252, h:100})
   .replace('Please enter your name: <br><input maxlength="23" id = "playerNameBox" TYPE="text" name="Name" value="' + randomName + ' " spellcheck=false />   ')
   .css({ 'font-size':'30px' , 'color': mycolors.greenDark});
  document.getElementById("playerNameBox").focus();
  document.getElementById("playerNameBox").select();

  // Rules popup
  // rulesPopup = Crafty.e('Rules')._Rules();


  startGame = function(){
    playerID = document.getElementById("playerNameBox").value;
    console.log('My name is: '+ playerID);

    //playerID = prompt("Please enter your name",names0[rand0]+names1[rand1]+names2[rand2]);// names0[rand0]+names1[rand1]+names2[rand2]);
    
    connect(playerID);
    Crafty.scene('Lobby');
  }

	 // start button
  Crafty.e('2D,DOM,Text, Mouse, Keyboard')
  .text('Enter the lobby')
  .textFont({ size: '35px', family: fontFamily2})
  .attr({ x: 275,  y:350, z:1008, w:250, h:50})
  .css({'background-color':mycolors.pink,'color':mycolors.buttontext, 'border-radius':'5px',
   'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
  .bind('Click', startGame)
  .bind('KeyDown', function() {  
    if(this.isDown('ENTER')){
      startGame();    
    }
   })
  .bind('MouseOver', function(){this.css({  'background-color':mycolors.orange,})})
  .bind('MouseOut', function(){this.css({  'background-color':mycolors.pink,  })});
	//.collision( new Crafty.circle(75,75,75));	
	
  
  // background plate
  Crafty.e('2D,Canvas,Image').image(imageMap['plate2']).attr({x:89, y:105, w:620, h:440, z:0});
  
  cakeStyle1 = [Math.floor(Math.random()*nrOfChoices), Math.floor(Math.random()*nrOfChoices), Math.floor(Math.random()*nrOfChoices)]; //[2,3,1];
  cakeStyle2 = [Math.floor(Math.random()*nrOfChoices), Math.floor(Math.random()*nrOfChoices), Math.floor(Math.random()*nrOfChoices)]; //0,2,3];
  // Cakes of players:
  //    the three layers of player 1
  cake1bottom = Crafty.e("2D, DOM, Tween, bottomlayerSprite").attr({x:40, y : -150, h:100, w:100, z:10}).sprite(cakeStyle1[0],0);
  cake1middle = Crafty.e("2D, DOM, Tween, middlelayerSprite").attr({x:40, y : -150, h:100, w:100,  z:20}).sprite(cakeStyle1[1],1);
  cake1topping =  Crafty.e("2D, DOM, Tween, toppingSprite").attr({x:40, y : -150, h:100, w:100, z:30}).sprite(cakeStyle1[2],2);
  cake1 = [cake1bottom, cake1middle, cake1topping];
  //    the three layers of player 2
  cake2bottom = Crafty.e("2D, DOM, Tween, bottomlayerSprite").attr({x:640, y : -150, h:100, w:100, z:10}).sprite(cakeStyle2[0],0);
  cake2middle = Crafty.e("2D, DOM, Tween, middlelayerSprite").attr({x:640, y : -150, h:100, w:100,  z:20}).sprite(cakeStyle2[1],1);
  cake2topping =  Crafty.e("2D, DOM, Tween, toppingSprite").attr({x:640, y : -150, h:100, w:100, z:30}).sprite(cakeStyle2[2],2);
  cake2 = [cake2bottom, cake2middle, cake2topping];
  // tiny trays to put them on :)
  Crafty.e("2D,DOM,Image").image(imageMap['petitfoursTinyTray']).attr({x:43, y:170, h:80, w:112, z:9});
  Crafty.e("2D,DOM,Image").image(imageMap['petitfoursTinyTray']).attr({x:643, y:170, h:80, w:112, z:9});

  for( var i = 0; i<3 ; i++){
    // tween/juice cakes
    cake1[i].tween({x: 30, w: 140,y:150},2000 + 30*i,easeOutBounce);
    cake2[i].tween({x: 630, w: 140,y:150},2000 + 30*i,easeOutBounce);
  }


	function connect(playerID) {
    // Initialise the local player
    localPlayer = new Player(playerID);
    // Start listening for events
    // socket = io("http://localhost:8000", {query: "playerID=" + playerID}); // local testing, don't forget to run your server!
    socket = io("http://localhost:8000", {query: "playerID=" + playerID});
    remotePlayers = [];
    setEventHandlers();
  }

  function setEventHandlers() {
    socket.on("connect", onSocketConnected);
    socket.on("disconnect", onSocketDisconnect);
    socket.on("new player", onNewPlayer);
    socket.on("remove player", onRemovePlayer);
  };

  function onSocketConnected() {
      console.log("Connected to socket server");
      // socket.emit("new player", {playerID: localPlayer.playerID});
  };

  function onSocketDisconnect() {
      console.log("Disconnected from socket server");
  };

  function onNewPlayer(data) {
      console.log("New player connected: " + data.playerID);
      var newPlayer = new Player();
      newPlayer.playerID = data.playerID;
      remotePlayers.push(newPlayer);
      listPlayers();
  };


  function onRemovePlayer(data) {
    var removePlayer = remotePlayers.find(x => x.playerID === data.playerID);

    if (!removePlayer) {
        console.log("Player not found: "+data.playerID);
        return;
    };

    remotePlayers.splice(remotePlayers.indexOf(removePlayer), 1);
    listPlayers();
  };

	// Crafty.scene('Lobby');
});
