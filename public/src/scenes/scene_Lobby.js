Crafty.scene('Lobby', function() {
	Crafty.timer.FPS(80);
    Crafty.background(mycolors.background);
	  
	// set some parameters
	availableGames = new Array();
	gameSelected = false;
	playerSelected = false;
	myCakeStyle = [Math.floor(Math.random()*nrOfChoices),Math.floor(Math.random()*nrOfChoices),Math.floor(Math.random()*nrOfChoices)];
	
    // Title
	title = Crafty.e('2D, DOM, Text').attr({     x: 200,  y:0 ,   w:400, h:80 ,z:108  })
		.text('Petit &nbsp Fours').textFont({ size: '53px',type: 'italic', family: fontFamily1})		
		.css({  'text-align':'center', 'text-shadow':'2px 2px '+ mycolors.titleshadow, 'color':mycolors.title});

	// Games
	gamesText =  Crafty.e('2D, DOM, Text').attr({     x: 100,  y:75 ,   w:300, h:60 ,z:108  })
		.text('Available games').textFont({ size: '27px',type: 'italic', family: fontFamily3})
		.css({'text-align':'center',  'color':mycolors.greenDark});	

	// Players
	playersText = Crafty.e('2D, DOM, Text').attr({   x: 450,  y:75 ,   w:250, h:60 ,z:108  })
		.text('Players').textFont({ size: '27px',type: 'italic', family: fontFamily3})
		.css({'text-align':'center',  'color':mycolors.greenDark});	

	// my name
	myNameText = Crafty.e('2D, HTML').attr({   x: 450,  y:400 ,   w:250, h:60 ,z:108  })
		.replace('<div id = "playersName" > '+ playerID + '</div>')
		//.text(playerID).textFont({ size: '27px',type: 'italic', family: fontFamily3})
		.css({'font-family': fontFamily3, 'font-size': '17px', 'text-align':'center',  'color':mycolors.greenDark});	

	// myNameDiv = document.getElementById("playersName");
	// var initialSize = 27 - playerID.length;
	// initialSize = initialSize <= 20 ? 20 : initialSize;
	// myNameDiv.style.fontSize = initialSize + "px";



	// position of my cake
	cakeX = 510;
	cakeY = 440;


	// Create Crafty HTML element, with a <div> in it to make list of games
	Crafty.e("HTML").attr({x:97, y:127, w:300, h:350}).replace('<div id = "gamesBox" class = "scrollFancy"> </div>   ');   
  	gamesBoxDiv = document.getElementById("gamesBox");

	// Create Crafty HTML element, with a <div> in it to make list of players
  	Crafty.e("HTML").attr({x:447, y:127, w:250, h:250}).replace('<div id = "playersBox" class = "scrollFancy"> </div>   ');   
  	playersBoxDiv = document.getElementById("playersBox");
  
	// Join game  button
	joinGameButton = Crafty.e('2D,DOM,Text, Mouse').attr({ x: 100,  y:500, z:1008, w:110, h:30})
	.text('Join this game')	.textFont({ size: '20px', family: fontFamily2})
	.css({'background-color':mycolors.pinkLight,'color':mycolors.buttontext, 'border-radius':'3px', 'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
	.bind('MouseOver',function(){
		var game2join = game2joinCheck();
		if(game2join != false){
			this.css({	'background-color':mycolors.orange, })
		}
		else{ 		}	
	})
	.bind('MouseOut',function(){
		var game2join = game2joinCheck();
		if(game2join){ 			this.css({	'background-color':mycolors.pink, }) 		}
		else{ 			this.css({	'background-color':mycolors.pinkLight, })			}			
	})
	.bind('Click', function() {			
		var game2join = game2joinCheck();		
		if (!game2join) {
			console.log("Select a game to join");
			infoBar.text("Select a game to join")
		} else {
			console.log('joining game ' + game2join);
			socket.emit('join game', {playerID: playerID, gameID: game2join, cakeStyle: myCakeStyle});
		}
	 });
	joinGameButton.highlight = function(){
		this.css({'background-color': mycolors.button1Highlight, 'cursor':'pointer'})
	};
	joinGameButton.unhighlight = function(){
		this.css({'background-color':mycolors.button1 ,'cursor':'default'});
	};

	game2joinCheck = function(){
		var game2join;		
		if(gameSelected != false){
			return gameSelected
		}
		return false
	};

	// Create a new game  button
	createGameButton = Crafty.e('2D,DOM,Text, Mouse, Keyboard').attr({ x: 215,  y:500, z:1008, w:120, h:30})
	.text('Create new game')	.textFont({ size: '20px', family: fontFamily2})
	.css({'background-color':mycolors.button1Highlight,'color':mycolors.buttontext, 'border-radius':'3px', 'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
	.bind('MouseOver',function(){this.css({	'background-color':mycolors.orange,'color':mycolors.buttontexthover, })})
	.bind('MouseOut',function(){this.css({	'background-color':mycolors.button1Highlight,  'color':mycolors.buttontext,})})
	.bind('Click', function() {		
		socket.emit('create game', {gameID: playerID + '\'s game', player1: playerID});
		// console.log("sock: " + socket);
	 })
	.bind('KeyDown', function() {	
		if(this.isDown('C')){	
			socket.emit('create game', {gameID: playerID + '\'s game', player1: playerID});
		}
	 });
	
	

	// Rules  button
	rulesButton = Crafty.e('2D,DOM,Text, Mouse').attr({ x: 340,  y:500, z:1008, w:60, h:30})
	.text('Rules').textFont({ size: '20px', family: fontFamily2})
	.css({'background-color':mycolors.button1Highlight,'color':mycolors.buttontext, 'border-radius':'3px', 'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
	.bind('MouseOver',function(){this.css({	'background-color':mycolors.orange,'color':mycolors.buttontexthover, })})
	.bind('MouseOut',function(){this.css({	'background-color':mycolors.button1Highlight,  'color':mycolors.buttontext,})})
	.bind('Click', function() {
		// Create a game function??
		// Show rules
		// Rules popup
  		rulesPopup = Crafty.e('Rules')._Rules();
		console.log('The rules page still has to be implemented.');
		infoBar.text('The rules page still has to be implemented.');
	 });
	


	// Show the cake that you currently have
	bottomlayer = Crafty.e("2D, DOM, bottomlayerSprite").attr({x:cakeX, y : cakeY, h:100, w:140, z:10}).sprite(myCakeStyle[0],0);
	middlelayer = Crafty.e("2D, DOM, middlelayerSprite").attr({x:cakeX, y : cakeY, h:100, w:140,  z:20}).sprite(myCakeStyle[1],1);
	topping =  Crafty.e("2D, DOM, toppingSprite").attr({x:cakeX, y : cakeY, h:100, w:140, z:30}).sprite(myCakeStyle[2],2);
	layers = [bottomlayer, middlelayer, topping];

	changeMyCake = function(layerIndex,diff,mod){
		myCakeStyle[layerIndex]  += diff + mod;
		myCakeStyle[layerIndex] = myCakeStyle[layerIndex] % mod;
		layers[layerIndex].sprite(myCakeStyle[layerIndex], layerIndex);
		//console.log('changing style of ' + layerIndex + ', myCakeStyle is now: ' + myCakeStyle);
	};


	// arrows to change style
	bottomArrowMinus = Crafty.e("Button")._Button(0, -1).attr({x: cakeX - 20, y: cakeY + 60}).flip("X");
	bottomArrowPlus = Crafty.e("Button")._Button(0, 1).attr({x: cakeX + 140, y: cakeY + 60});
	middleArrowMinus = Crafty.e("Button")._Button(1, -1).attr({x: cakeX - 20, y: cakeY + 35}).flip("X");
	middleArrowPlus = Crafty.e("Button")._Button(1, 1).attr({x: cakeX + 140, y: cakeY + 35});
	topArrowMinus = Crafty.e("Button")._Button(2, -1).attr({x: cakeX - 20, y:cakeY + 10}).flip("X");
	topArrowPlus = Crafty.e("Button")._Button(2, 1).attr({x: cakeX + 140, y:cakeY + 10});
	// tiny tray
	Crafty.e("2D,DOM,Image").image(imageMap['petitfoursTinyTray']).attr({x:cakeX + 13, y:cakeY + 17, h:80, w:112, z:9});


  
  	// info bar at bottom
  	infoBar = Crafty.e('2D,DOM,Text').attr({x:0, y:570, h:30,w:800})
  	.text('This is an optional info bar. Says "game created" or "game is already full", "game finished" etc.')
	.textFont({ size: '20px', family: fontFamily2})
	.css({'background-color':'#DED7DD','color':'#5E2C5A',  'cursor': 'default'});//'text-shadow':'1px 1px ' + mycolors.pink,


	function updateGameList(newGameList){
		availableGames = newGameList;
		listAvailableGames();
	};

	function updatePlayerList(newPlayerList){
		remotePlayers = newPlayerList;
		listPlayers();
	};



	function listAvailableGames(){	// something has to be done when games disappears when it is has been deleted
			// something has to show that a game is full
		
		var tempHtml = '<ul>'; 
		for(var i = 0; i < availableGames.length; i++){
			var gameID = availableGames[i];
						
			if(gameID === gameSelected){
				console.log('SAME GAMEEE');
				tempHtml += '<li class="selected gameLine">' + gameID + '</li>';
			}
			else{	
				tempHtml += '<li class="unselected gameLine">'  + gameID + '</li>';
			}
			
		}
		

		tempHtml += '</ul>';
		gamesBoxDiv.innerHTML = tempHtml;

		// add a click function to each gameline
		var gameLines = document.getElementsByClassName('gameLine');			
		for(var i=0; i< gameLines.length; i++){
			var gameLine = gameLines[i];

			// click = select this game
			gameLine.addEventListener("mouseup", function(){
			  	if(this.classList.contains('selected')){ // unselect this one
			  		this.classList.add('unselected');
					this.classList.remove('selected');	
					joinGameButton.unhighlight();
					gameSelected = false;
			  	}
			  	else{// select this one!
			  		joinGameButton.highlight();
			  		gameSelected = this.innerHTML;
			  		console.log('game selected: '+ gameSelected);
			  		
					// deselect other selected game line, if there is one
					previousSelected =  document.getElementsByClassName('gameLine selected');		  	
					console.log('Previous Selected: ' + previousSelected + ' with length'+ previousSelected.length );
					if(previousSelected.length){
						var prev =	previousSelected[0];
						prev.classList.remove('selected');
						prev.classList.add('unselected');
					}
					// select this
					this.classList.add('selected');
					
					this.classList.remove('unselected');	
			  	}
			  	console.log('Gameline Clicked!' + this.innerHTML);
			});
		}
	};


	listPlayers = function(){ // something has to be done when player leaves that was selected
	
		// make an <uL> element with every player in an <li>
		var tempHtml = '<ul>'; 
		for(var i = 0; i < remotePlayers.length; i++){
			var remotePlayer = remotePlayers[i];
			var remotePlayerID = remotePlayer.playerID;						
			if(remotePlayerID === playerSelected){				
				tempHtml += '<li class="playerLine selected">' + remotePlayerID + '</li>';
			}
			else{	
				tempHtml += '<li class="playerLine unselected">'  + remotePlayerID + '</li>';
			}			
		}
		tempHtml += '</ul>';
		playersBoxDiv.innerHTML = tempHtml;

		// add a click function to each player's name
		var playerLines = document.getElementsByClassName('gameLine');			
		for(var i=0; i< playerLines.length; i++){
			var playerLine = playerLines[i];

			// click = select this game
			playerLine.addEventListener("mouseup", function(){
				console.log('You clicked on player ' + this.innerHTML);
			  	if(this.classList.contains('selected')){ // unselect this player, as a result no one is selected
			  		this.classList.add('unselected');
					this.classList.remove('selected');						
					playerSelected = false;
			  	}
			  	else{// select this player	  		
					// first deselect other selected player, if existing
					previousSelected =  document.getElementsByClassName('playerLine selected');		  	
					if(previousSelected.length){
						var prev =	previousSelected[0];
						prev.classList.remove('selected');
						prev.classList.add('unselected');
					}
					// now select the clicked player
					this.classList.add('selected');					
					this.classList.remove('unselected');	
					playerSelected = this.innerHTML;
			  		console.log('You selected player: '+ gameSelected);
			  	}
			  	
			  	
			});
		}
	};




	function onUpdateGames(data) {
		console.log('gameIDs received; ' + data.gameIDs);
		updateGameList(data.gameIDs);
	}

	function onUpdatePlayers(data) {
		console.log('playerIDs received; ' + data.playerIDs);
		updatePlayerList(data.playerIDs);
	}

	function onCakePlease(data) {
		socket.emit('submit cake', {cakeStyle: myCakeStyle});
	}

	function onJoinGame(data) {
		console.log("joined game! : " + data.gameID);
		joinedGameID = data.gameID;
		playerStoneType = data.stoneType;
		cakeStyles = data.cakeStyles;
		playingPlayers = data.players;
		console.log('playing Players: ' + playingPlayers);
		console.log('cake styles: ' + cakeStyles);
		console.log('My stoneType: ' + playerStoneType);
		
		Crafty.scene("Main");
	}

	socket.on("update games", onUpdateGames);
	socket.on("cake please", onCakePlease);
	socket.on("join game", onJoinGame);

	
	listAvailableGames();
	listPlayers();

	


});

