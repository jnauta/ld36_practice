// LOADING SCENE
Crafty.scene('Loading', function() {
	// // Draw some text for the player to see in case the file
	// //  takes a noticeable amount of time to load

	//Crafty.background('url(assets/images/background1.png)');
    Crafty.background(mycolors.background);
	
	Crafty.e('2D, DOM, Text')
		.attr({
			x: 321,
			y: 188,
			w: 222,
			h: 200,
			z: 8
		})
		.textFont({
			size: '24px',
			family: fontFamily1
		})
		.css({
			'padding-top': '45px',
			'background-color': mycolors.textBlockBg,
			'border': ('2px dashed' + mycolors.button1),
			'border-radius': '8px',
			'cursor': 'wait',
			'text-align': 'center',
			'padding-top': '3px',
			'color':mycolors.textBlockText,
		})
		.text('<BR><br>Loading,<br><BR> please wait...');
	Crafty.load(assetsObject,
		function() {
			//when loaded
			console.log('everything loaded!');
				
			Crafty.sprite(50, 50, "assets/images/petitfours.png", {
				petitfour1old: [0,0],		
				petitfour2old: [1,0],
			});

			Crafty.sprite(70, 50, "assets/images/petitfoursiso.png", {
				petitfour1: [0,0],		
				petitfour2: [1,0],
			});

			Crafty.sprite(140, 100, "assets/images/petitfourslayers.png", {
				bottomlayerSprite: [0,0],		
				middlelayerSprite: [0,1],
				toppingSprite: [0,2],
			});

			Crafty.sprite(140, 100, "assets/images/highlight.png", {
				highlight: [0,0],					
			});

			Crafty.sprite(15, 20, "assets/images/arrows.png", {
				arrow: [0,0],						
			});

			Crafty.sprite(45, 60, "assets/images/arrowsBig.png", {
				arrowBig: [0,0],						
			});

			Crafty.sprite(40, 40, "assets/images/exitbutton.png", {
				exitButton: [0,0],						
			});
			 
			// Crafty.sprite(24, 24, "assets/images/tiles.png", {
				// solidTile: [0,0],
			// });
			
			// Crafty.sprite(48, 48, "assets/images/robot_knuppel.png", {
				// baddie1: [0,0],			
			// });

			// Crafty.sprite(224, 192, "assets/images/boss.png", {
				// boss: [0,0],			
			// });
			
			// Crafty.sprite(1,1, "assets/images/bubbles_alpha.png", {
				// bubble1: [0,0,24,24],			
				// bubble2: [0,24,48,48],			
				// bubble3: [0,72,72,72],			
			// });
			
			// Crafty.sprite(92,128, "assets/images/screen.png", {
				// screen: [0,0],
					
			// });
			
			// Crafty.sprite(20,20, "assets/images/misc/music.png", {
				// music: [0,0],
					
			// });
						
			// Crafty.sprite(24,24, "assets/images/smurrie.png", {
				// smurrie1: [0,0],
				// smurrie2: [1,0],							
				// smurrie3: [2,0],							
				// smurrie4: [3,0],							
				
			// });			
			
			// Crafty.sprite(72,72, "assets/images/bubble_grow.png", {
				// grow: [0,0],	
				
			// });			
			
			
			// Crafty.scene('Main');
			// Crafty.scene('Lobby');
			Crafty.scene('StartScreen');
		},

		function(e) { // onProgress
			//console.log(e);
		},

		function(e) {
			console.log('loading error');
			console.log(e);
		}
	);
    
    
    

});