var overlay;
var bgMusic = null;

// LOADING SCENE
Crafty.scene('Main', function() {
  gameOver = false;
  // Crafty.background(mycolors.background);
  Crafty.timer.FPS(80);
  //Crafty.viewport.bounds = {min:{x:0, y:0}, max:{x:Game.width(), y: Game.height()}};

  // if (!bgMusic) {
  // bgMusic = Crafty.audio.play('bgMusic',-1,0.3);
  // if(mutemusic && bgMusic.source){
  // bgMusic.pause();
  // }
  // }

  // Letters for board
  letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
  // Title
  Crafty.e('2D, DOM, Text, Mouse').attr({   x: 200,  y: 0,  w: 400,  h: 60,  z: 108})
  .text('Petit &nbsp Fours').textFont({  size: '53px',  type: 'italic',  family: fontFamily1})
  .css({  'padding-top': '5px',  'text-align': 'center',  'text-shadow': '2px 2px ' + mycolors.titleshadow,  'color': mycolors.title});

  // info bar at bottom
  infoBar = Crafty.e('2D,DOM,Text').attr({x:0, y:570, h:30,w:800})
  .text('This is an info bar. Says "game created" or "game is already full", "game finished" etc.')
  .textFont({ size: '20px', family: fontFamily2})
  .css({'background-color':'#DED7DD','color':'#5E2C5A',  'cursor': 'default'});//'text-shadow':'1px 1px ' + mycolors.pink,

    // Rules  button
  rulesButton = Crafty.e('2D,DOM,Text, Mouse').attr({ x: 500,  y:525, z:1008, w:60, h:30})
  .text('Rules').textFont({ size: '20px', family: fontFamily2})
  .css({'background-color':mycolors.button1Highlight,'color':mycolors.buttontext, 'border-radius':'3px', 'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
  .bind('MouseOver',function(){this.css({ 'background-color':mycolors.orange,'color':mycolors.buttontexthover, })})
  .bind('MouseOut',function(){this.css({  'background-color':mycolors.button1Highlight,  'color':mycolors.buttontext,})})
  .bind('Click', function() {
    // Create a game function??
    // Show rules
    // Rules popup
      rulesPopup = Crafty.e('Rules')._Rules();
    console.log('The rules page still has to be implemented.');
    infoBar.text('The rules page still has to be implemented.');
   });

  // Give up  button
  giveUpButton = Crafty.e('2D,DOM,Text, Mouse').attr({ x: 250,  y:525, z:1008, w:60, h:30})
  .text('Give up').textFont({ size: '20px', family: fontFamily2})
  .css({'background-color':mycolors.button1Highlight,'color':mycolors.buttontext, 'border-radius':'3px', 'text-shadow':'1px 1px '+ mycolors.buttontextshadow,'cursor': 'pointer'})
  .bind('MouseOver',function(){this.css({ 'background-color':mycolors.orange,'color':mycolors.buttontexthover, })})
  .bind('MouseOut',function(){this.css({  'background-color':mycolors.button1Highlight,  'color':mycolors.buttontext,})})
  .bind('Click', function() {
    Crafty.scene('Lobby');

      
    console.log('The rules page still has to be implemented.');
    infoBar.text('Giving up is not yet possible. Keep playing.');
   });

  // background plate
  Crafty.e('2D,Canvas,Image').image(imageMap['plate2']).attr({
    x: 89,
    y: 105,
    w: 620,
    h: 440,
    z: 0
  });

  // Display names of players
  namePlayer1 = Crafty.e('2D,DOM,Text').css({
      'color': mycolors.greenDark,
      'text-align': 'left'
    }).attr({
      x: 50,
      y: 100,
      h: 40,
      w: 200
    })
    .text('Player 1').textFont({
      size: '23px',
      type: 'italic',
      family: fontFamily3
    });
  namePlayer2 = Crafty.e('2D,DOM,Text').css({
      'color': mycolors.greenDark,
      'text-align': 'right'
    }).attr({
      x: 550,
      y: 100,
      h: 40,
      w: 200
    })
    .text('Player 2').textFont({
      size: '23px',
      type: 'italic',
      family: fontFamily3
    });
  playerNames = [namePlayer1, namePlayer2];
  playerNames[0].text( playingPlayers[playerStoneType-1]); // myself left
  playerNames[1].text( playingPlayers[2- playerStoneType]); // opponent on the right

  // Cakes of players:
  // the three layers of player 1, stoneTypes are 1 or 2.
  var myCake = cakeStyles[playerStoneType - 1];
  var oppCake = cakeStyles[2 - playerStoneType];
  cake1bottom = Crafty.e("2D, DOM, bottomlayerSprite").attr({
    x: 30,
    y: 150,
    h: 100,
    w: 140,
    z: 10
  }).sprite(myCake[0], 0);
  cake1middle = Crafty.e("2D, DOM, middlelayerSprite").attr({
    x: 30,
    y: 150,
    h: 100,
    w: 140,
    z: 20
  }).sprite(myCake[1], 1);
  cake1topping = Crafty.e("2D, DOM, toppingSprite").attr({
    x: 30,
    y: 150,
    h: 100,
    w: 140,
    z: 30
  }).sprite(myCake[2], 2);
  //    the three layers of player 2
  cake2bottom = Crafty.e("2D, DOM, bottomlayerSprite").attr({
    x: 630,
    y: 150,
    h: 100,
    w: 140,
    z: 10
  }).sprite(oppCake[0], 0);
  cake2middle = Crafty.e("2D, DOM, middlelayerSprite").attr({
    x: 630,
    y: 150,
    h: 100,
    w: 140,
    z: 20
  }).sprite(oppCake[1], 1);
  cake2topping = Crafty.e("2D, DOM, toppingSprite").attr({
    x: 630,
    y: 150,
    h: 100,
    w: 140,
    z: 30
  }).sprite(oppCake[2], 2);
  // tiny trays to put them on :)
  Crafty.e("2D,DOM,Image").image(imageMap['petitfoursTinyTray']).attr({
    x: 43,
    y: 170,
    h: 80,
    w: 112,
    z: 9
  });
  Crafty.e("2D,DOM,Image").image(imageMap['petitfoursTinyTray']).attr({
    x: 643,
    y: 170,
    h: 80,
    w: 112,
    z: 9
  });


  

  // Text above chat window
  Crafty.e('2D,DOM,Text').css({
      'color': mycolors.greenDark,
      'text-align': 'right'
    }).attr({
      x: 580,
      y: 420,
      h: 40,
      w: 170
    })
    .text('Chat').textFont({
      size: '23px',
      type: 'italic',
      family: fontFamily3
    });
  // Create Crafty HTML element, with a <div> in it to make a chat window
  Crafty.e("HTML").attr({
    x: 580,
    y: 450,
    w: 170,
    h: 100
  }).replace('<div id = "chatDiv" class = "scrollFancy">Me: hoi! <br> Opp: Hallo :) <br> Me: This not really a chat yet, but OK. <br> Opp: No... but it is fun! </div>   ');
  chatDiv = document.getElementById("chatDiv");


  // Text above log window
  Crafty.e('2D,DOM,Text').css({
      'color': mycolors.greenDark,
      'text-align': 'left'
    }).attr({
      x: 50,
      y: 415,
      h: 40,
      w: 200
    })
    .text('Log window').textFont({
      size: '23px',
      type: 'italic',
      family: fontFamily3
    });
  // HTML element Log window
  Crafty.e("HTML").attr({
    x: 50,
    y: 450,
    w: 170,
    h: 100
  }).replace('<div id = "logDiv" class = "scrollFancy"> <ul id = "logList"></ul>   ');
  logDiv = document.getElementById("logDiv");
  logList = document.getElementById("logList");

  logList.innerHTML = '<li> Game started </li>';
  logCount = 1;

  logClear = function(){
    logList.innerHTML = '<li> Game started </li>';
    logCount = 1;
  };
  logAppend = function(message){
    logList.innerHTML += '<li>' + logCount + '. ' + message + '</li>';
    logDiv.scrollTop = logDiv.scrollHeight;
    logCount += 1;
  };
  


  





  // game parameters, move later elsewhere
  tilew = 70;
  tileh = 50;

  gameSizeTileX = 8;
  gameSizeTileY = 8;

  startTileX = 200;
  startTileY = 125;

  // isometric grid
  iso = Crafty.diamondIso.init(70, 50, 10, 10, 400, 75);



  // initialize 2-dimensional array of the tiles
  Tiles = new Array(gameSizeTileX);
  for (var i = 0; i < gameSizeTileX; i++) {
    Tiles[i] = new Array(gameSizeTileY);
  }
  // fill with tiles
  for (var i = 0; i < gameSizeTileX; i++) {
    for (var j = 0; j < gameSizeTileY; j++) {
      Tiles[i][j] = Crafty.e("Tile")._Tile(i, j, playerStoneType, cakeStyles).attr({
        z: (i + 1) * (j + 1)
      });
      //Tiles[i][j].changeCakeStyle(cakeStyles[playerStoneType]);
      // place them in the isometric grid
      iso.place(Tiles[i][j], i, j, 0);
    }
  }
  wantedMove = [-1, -1];
  turnStatus = "regular";
  displacePos = {x: -2, y: -2};
  // both players start with 0 Connect Fours:
  // connect4CountPlayer1 = 0;  
  // connect4CountPlayer2 = 0;  

  changeWantedMove = function(x, y) {
    if (wantedMove[0] !== -1) {
      Tiles[wantedMove[0]][wantedMove[1]].wanted = false;
      Tiles[wantedMove[0]][wantedMove[1]].stopBlink();
      // console.log('stop blinking, another is wanted');
    }
    if (x !== -1) {
      wantedMove = [x, y];
      console.log('wanted move: ' + [x, y]);
      Tiles[x][y].wanted = true;
      Tiles[x][y].startBlink();
    }
  }

  function onPlayerMove(data) {
    decreaseCountdowns();
    // set cakestyle of empty tiles to that of the local player, so that hover-overs show in the right style.
    setCakeStyleEmptyTiles(cakeStyles[playerStoneType - 1]);
    // stop blinking, whatever this move does resets the player's wanted move

    changeWantedMove(-1, -1);
    // by default, both players go into the regular state after a move, where both may play
    turnStatus = "regular";
    if (data.turnType === "regular") {
      
      placeTile([data.move1.xpos, data.move1.ypos], 1);
      placeTile([data.move2.xpos, data.move2.ypos], 2);
      // after they arrived, budge other tiles

      for (var i = 0; i < Tiles.length; i++) {
        for (var j = 0; j < Tiles[i].length; j++) {
          var tile = Tiles[i][j];
          if (tile.stoneType != 0 && (tile.xpos != data.move1.xpos && tile.ypos != data.move1.ypos) ) {
            tile.delay(function(){this.budge([data.move1.xpos,data.move1.ypos])},700,0);
          }
        };
      }


      // For displaying text in log window,  such that your own move is mentioned first
      var movePlayer1 = letters[data.move1.xpos] + (data.move1.ypos+1);
      var movePlayer2 = letters[data.move2.xpos] + (data.move2.ypos+1);
      var moveText = '';
      if(playerStoneType === 1 ){ 
        moveText = movePlayer1 + ', '+ movePlayer2;
      } else{
        moveText = movePlayer2 + ', '+ movePlayer1;
      }
      logAppend(moveText);
    }

    else if (data.turnType === "beat") {
      placeTile([data.move.xpos, data.move.ypos], data.stoneType);
      var moveText = letters[data.move.xpos] + (data.move.ypos + 1)  
      if (data.stoneType === playerStoneType) {
        turnStatus = "displace"; // this player must displace the opponent's stone.
        displacePos.x = data.move.xpos;
        displacePos.y = data.move.ypos;
        setCakeStyleEmptyTiles(oppCake);
        // setCakeStyleEmptyTiles(cakeStyles[2 - playerStoneType]);
        logAppend("Beat: " +  moveText + ". Move the cake of the opponent!" );

        // highlight tiles that can be used for displacement
         
        for (var i = 0; i < Tiles.length; i++) {
          for (var j = 0; j < Tiles[i].length; j++) {
            var tile = Tiles[i][j];
            if (tile.stoneType === 0 && Math.abs(tile.xpos - displacePos.x) <= 1 && Math.abs(tile.ypos - displacePos.y) <= 1) {
              tile.highlightPic.tween({alpha:0.6},400);
            }
          };
        }

        // WHAT IF THERE ARE NO EMPTY TILES TO DISPLACE TO?
      } else {
        turnStatus = "waitForDisplace"; // the other player must displace this player's stone.
        logAppend("Beat: " +  moveText + ". Wait for other player to move your cake");
      }
    } else if (data.turnType === "displace") {
      if (data.move1) {
        placeTile([data.move1.xpos, data.move1.ypos], 2); // displace opponent's stone
        logAppend("Cake got displaced to " + letters[data.move1.xpos] + (data.move1.ypos+1) );
      }
      if (data.move2) {
        placeTile([data.move2.xpos, data.move2.ypos], 1); // displace opponent's stone
        logAppend("Cake got displaced to " + letters[data.move2.xpos] + (data.move2.ypos+1) );
      }
    } else if (data.turnType === "sentinel") {
      if (data.move1) {
        placeTile([data.move1.xpos, data.move1.ypos], 1);
        logAppend("Sentinel!");
      }
      if (data.move2) {
        placeTile([data.move2.xpos, data.move2.ypos], 2);
        logAppend("Sentinel!");
      }
    } else if (data.turnType === "tie") {
      placeTile([data.move.xpos, data.move.ypos], data.move.stoneType);
      logAppend("Tie at " + letters[data.move.xpos] + (data.move.ypos+1) );
    }
  }

  function onPlayerWin(data) {
    console.log('Do we have a winner? ' + data.winner);
    gameOver = true;
    if (data.winner === playerStoneType) {
      console.log("You Win!");
      logAppend("You Win!");
    } else if (data.winner === 3 - playerStoneType) {
      console.log("You Lose!");
      logAppend("You Lose!");
    } else if (data.winner === 0) {
      console.log("It's a Draw!");
      logAppend("It's a Draw!");
    }
  }

  function placeTile(position, stoneType) {
    tile = Tiles[position[0]][position[1]];
    tile.css({
      'cursor': 'default'
    });

    tile.setStoneType(stoneType);
    // I have put stop blink here, maybe there is a better place or way to do it
    tile.stopBlink();
    //computeConnect4([tile.xpos,tile.ypos]);

    // console.log('colour: ' + data.colorIdx);
    //tile.petitfour.sprite(data.colorIdx - 1,0);


    //   Change CakeStyle if its opponents move: !!!!!!!!
    // if(tile){tile.changeCakeStyle(cakeStyleOpp);}

    tile.petitfour[0].attr({
      alpha: 1,
      y: tile.h - 150
    }).tween({
      x: tile.x,
      y: tile.y
    }, 1600,easeOutBounce);
    tile.petitfour[1].attr({
      alpha: 1,
      y: tile.h - 200
    }).tween({
      x: tile.x,
      y: tile.y
    }, 1700, easeOutBounce);
    tile.petitfour[2].attr({
      alpha: 1,
      y: tile.h - 250
    }).tween({
      x: tile.x,
      y: tile.y
    }, 1800,easeOutBounce);


    // animate incoming cake
    for(var i  = 0; i<3 ; i++){
      var cakeLayer = tile.petitfour[i];
      cakeLayer.attr({       alpha: 1,   x:tile.x+15,   w: 40, y: -tile.h      })
      .tween({       x: tile.x,   w:70,    y: tile.y     }, 1700 + 30 * i, easeOutBounce);
    }
    



   



  }

  function decreaseCountdowns() {
    for (var i = 0; i < Tiles.length; i++) {
      for (var j = 0; j < Tiles[i].length; j++) {
        var tile = Tiles[i][j];
        if (tile.stoneType > 2) {
          tile.decreaseCountdown();
        }
      };
    }
  }

  // sets style of all empty tiles to the cakeStyle parameter.
  // alpha is still zero, so this is only visible on mouse hover.
  function setCakeStyleEmptyTiles(cakeStyle) {
    for (var i = 0; i < Tiles.length; i++) {
      for (var j = 0; j < Tiles[i].length; j++) {
        var tile = Tiles[i][j];       
        tile.highlightPic.tween({alpha:0},200);
        
        if (tile.stoneType === 0) {
          tile.setCakeStyle(cakeStyle);
        }
      };
    }
  };

  socket.on("player move", onPlayerMove);
  socket.on("player win", onPlayerWin);
});