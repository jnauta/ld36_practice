# Petit Fours

**Upon cloning this repository, and whenever you add/remove/change any assets, run the build.py script in the public directory.**

## Starting the server

To setup the game run `node serverGame.js` in the root directory, and run a web server to serve the public directory (e.g. `python3 -m http.server 8001` in the public directory).

The players can access the game on localhost:8001 (or whatever port you chose), and after they enter their name they connect to the node server on localhost:8000 to enter the lobby, from which they can host/join a game.

## Game Rules

The game proceeds in lockstep, i.e. both players must choose a legal move before both moves are revealed (except in special cases).

//TODO