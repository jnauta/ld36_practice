var ServerTile = require('./ServerTile.js').ServerTile;

var GameInstance = function(gameID, host, joiner){
  this.gameID = gameID;
  this.player1 = host;
  this.player2 = joiner;
  this.gameOver = false;
  this.cakeStyles = [null, null];

  // reset after each turn
  this.move1 = null;
  this.move2 = null;
  this.prevMove1 = null;
  this.prevMove2 = null;
  
  // persistent variables
  // both players start with 0 Connect Fours:
  this.scores = [0, 0];
  this.tiePos = null; // position to record the position of the last tie.
  this.tieStreak = 0; // used to increase timeout in subsequent ties.
  this.displacers = [false, false]; // indicates which players must displace-move before normal play continues.
  this.displaceOrigin = [-1, -1]; // used to force displacement to an adjacent tile, and not just any tile.
  // initialize 2-dimensional array of the tiles
  this.gameSizeTileX = 8;
  this.gameSizeTileY = 8;
  this.tiles = new Array(this.gameSizeTileX);
  for (var i = 0 ; i < this.gameSizeTileX; i++ ){
    this.tiles[i] = new Array(this.gameSizeTileY);
    for (var j = 0 ; j < this.gameSizeTileY; j++ ){
      this.tiles[i][j] =  new ServerTile(i,j,0);
    }
  }

  GameInstance.prototype.submitMove = function(data) {
    console.log('game over: ' + this.gameOver);
    if (!this.gameOver) {
      console.log('game not over, considering move for stonetype ' + data.stoneType);
      // displacer[n] (boolean) corresponds with player n-1
      // stonetype corresponds with the player who submitted the move,
      // even if it is e.g. a displacement move (which results in the opponent's stonetype being placed).
      if (data.stoneType === 1 && (this.displacers[0] || !this.displacers[1])) {
        this.move1 = data;
      } else if (data.stoneType === 2 && (this.displacers[1] || !this.displacers[0])) {
        this.move2 = data;
      }
    }
  }

  // process the two moves of this turn, adapt the game state.
  // return a result which may directly be emitted to the clients.
  GameInstance.prototype.processMoves = function() {
    var move1 = this.move1;
    var move2 = this.move2;
    var tiles = this.tiles;
    var turnResult = {};

    this.returnTurn = function(turn) {
      // an actual turn is returned, decrease the counters on tie pie's
      for (var i = 0 ; i < this.gameSizeTileX; i++ ){
        for (var j = 0 ; j < this.gameSizeTileY; j++ ){
          this.tiles[i][j].decreaseCountdown();
        }
      }
      this.prevMove1 = this.move1;
      this.prevMove2 = this.move2;
      this.move1 = null;
      this.move2 = null;
      console.log('returning turn of type ' + turn.turnType);
      return turn;
    }

    // do we have enough input to process?
    console.log("moves: " + this.move1 + " and " + this.move2);
    if ((move1 && move2) ||
        (move1 && this.displacers[0] && !this.displacers[1]) ||
        (move2 && this.displacers[1] && !this.displacers[0])) {
      if (this.displacers[0] || this.displacers[1]) {
        //this is a displacement move
        // in each case, first check that displacement(s) is legal, i.e. to an adjacent tile
        if (move1 && !move2) {
          if (Math.abs(move1.xpos - this.displaceOrigin[0]) <= 1 && Math.abs(move1.ypos - this.displaceOrigin[1]) <= 1) {
            tiles[move1.xpos][move1.ypos].stoneType = 2; // displace opponent's stone
            var extraScores = this.computeConnect4(move1.xpos, move1.ypos);
            this.scores[1] += extraScores[1];
          } else {
            console.log("illegal displacement!");
            return null; // illegal "displacement"
          }
        } else if (move2 && !move1) {
          if (Math.abs(move2.xpos - this.displaceOrigin[0]) <= 1 && Math.abs(move2.ypos - this.displaceOrigin[1]) <= 1) {
            tiles[move2.xpos][move2.ypos].stoneType = 1;
            var extraScores = this.computeConnect4(move2.xpos, move2.ypos);
            this.scores[0] += extraScores[0];
          } else {
            console.log("illegal displacement!");
            return null; // illegal "displacement"
          }
        } else {
          if (Math.abs(move1.xpos - this.displaceOrigin[0]) + Math.abs(move1.ypos - this.displaceOrigin[1]) === 1
            && Math.abs(move2.xpos - this.displaceOrigin[0]) + Math.abs(move2.ypos - this.displaceOrigin[1]) === 1) {
            if (move1.xpos === move2.xpos && move1.ypos === move2.ypos) {
              tiles[move1.xpos][move1.ypos].stoneType = 3;
              this.tieStreak++;
              // players displaced to the same tile
              return this.returnTurn({turnType: "tie", move: {xpos: move1.xpos, ypos: move1.ypos, stoneType: 2 + this.tieStreak}});
            }
          } else {
            return null; // illegal "displacement"
          }
        }
        // copy displacers so we may reset them (no 2 displacements in a row).
        var displacers = [this.displacers[0], this.displacers[1]];
        this.displacers = [false, false];
        this.tieStreak = 0;

        return this.returnTurn({turnType: "displace", move1: move1, move2: move2,
                displacers: displacers, scores: this.scores});
      } else { // both players played a move
        // regular move with both players
        if (move1.xpos === move2.xpos && move1.ypos === move2.ypos) {
          var xpos = move1.xpos;
          var ypos = move1.ypos;
          tiePos = [xpos, ypos]; // remember for next turn
          var result = this.computeShortestStretch(xpos, ypos);
          var stoneType = result[0];
          var connectLength = result[1];
          if (stoneType === 0) {
            // tie, place a timeoutMarker, no points are scored.
            tiles[xpos][ypos].stoneType = 3;
            this.tieStreak++;
            return this.returnTurn({turnType: "tie", move: {xpos: move1.xpos, ypos: move1.ypos, stoneType: 2 + this.tieStreak}});
          } else {
            // "beat", one of the players may displace the other one's stone.
            this.displaceOrigin = [xpos, ypos];
            tiles[xpos][ypos].stoneType = stoneType;
            var extraScores = this.computeConnect4(move1.xpos, move1.ypos); // move1 and move2 at same location
            this.scores[stoneType - 1] += extraScores[stoneType - 1];
            var move;
            if (stoneType == 1) {
              move = move1;
            } else {
              move = move2;
            }
            this.tieStreak = 0;
            this.displacers[stoneType - 1] = true; // this player must displace the opponent's stone
            return this.returnTurn({turnType: "beat", move: move, stoneType: stoneType, scores: this.scores});
          }
        } else {
          this.tieStreak = 0;
        // players played on different tiles.
        // check sentinels
          if (this.onOneLine(move1, move2)){
            // first determine whether one of the players has connected four.
            // scores are not added, since the players must displace.
            var connects = this.computeConnect4(move1.xpos, move1.ypos);
            var score1 = connects[0];
            connects = this.computeConnect4(move2.xpos, move2.ypos);
            var score2 = connects[1];
            if (score1 > 0 && score2 === 0) {
              // player2 may displace player1
              //this.displacers[1] = true;
              move1 = null;
            } else {
              tiles[move1.xpos][move1.ypos].stoneType = 1;
              var extraScores = this.computeConnect4(move1.xpos, move1.ypos); // move1 and move2 at same location
              this.scores[0] += extraScores[0];
            }
            if (score2 > 0 && score1 === 0) {
              // player1 may displace player2
              //this.displacers[0] = true;
              move2 = null;
            } else {
              tiles[move2.xpos][move2.ypos].stoneType = 2;
              var extraScores = this.computeConnect4(move2.xpos, move2.ypos); // move1 and move2 at same location
              this.scores[1] += extraScores[1];
            }
            // did any sentinel action happen?
            if (score1 > 0 || score2 > 0) {
              // sentinel'd turns are set to null.
              return this.returnTurn({turnType: "sentinel", move1: move1, move2: move2, displacers: this.displacers, scores: this.scores});
            }
          }
          tiles[move1.xpos][move1.ypos].stoneType = 1;
          tiles[move2.xpos][move2.ypos].stoneType = 2;
          var score1 = this.computeConnect4(move1.xpos, move1.ypos);
          var score2 = this.computeConnect4(move2.xpos, move2.ypos);
          this.scores[0] += score1[0];
          this.scores[1] += score2[1];
          return this.returnTurn({turnType: "regular", move1: move1, move2: move2, scores: this.scores});
        }
      }
    }
    // not enough data yet, waiting for other player.
    return null;
  }

  GameInstance.prototype.decideWinner = function() {
    if (!this.displacers[0] && !this.displacers[1]) {
    // we are not waiting for any displacers
      if (this.scores[0] > this.scores[1]) {
        this.gameOver = true;
        return 1;
      } else if (this.scores[1] > this.scores[0]) {
        this.gameOver = true;
        return 2;
      } else {
        // the board may be full, then we have a draw.
        var emptyTileCount = 0;
        for (var i = 0; i < this.tiles.length; i++) {
          tileArray = this.tiles[i];
          for (var j = 0; j < tileArray.length; j++) {
            if (tileArray[j].stoneType === 0) {
              emptyTileCount++;
            }
          };
          if (emptyTileCount === 0) {
            this.gameOver = true;
          }
        };
      }
    }
    return null; // no winner
  }

  GameInstance.prototype.onOneLine = function(move1, move2) {
    if (move1.xpos === move2.xpos || // horizontal
        move1.ypos === move2.ypos || // vertical
        Math.abs(move1.xpos - move2.xpos) === Math.abs(move1.ypos - move2.ypos)) { // diagonal
      return true;
    }
    return false;
  }

  // Returns which stonetype has the shortest connect containing (baseX, baseY) if playing
  // at this position. Used to resolve draws.
  // Output: [winStoneType, connectlength].
  // In case of a tie, winStoneType = 0, connectLength is set to the tied length.
  GameInstance.prototype.computeShortestStretch = function(baseX, baseY) {
    var tiles = this.tiles;
    // to keep track of the largest connects per player.
    var maxPlayer1 = maxPlayer2 = 1;
    var stretches = new Array();
    stretches.push([[0, 0, 0], [0, 0, 0], [0, 0, 0]]); // array for player 1.
    stretches.push([[0, 0, 0], [0, 0, 0], [0, 0, 0]]); // array for player 2.

    for (var i = -1; i < 2; i++) {
      for (var j = -1; j < 2; j++) {
        if (i === 0 && j === 0) {
          continue;
        }
        stoneCounter = 0;
        var xpos = baseX;
        var ypos = baseY;
        var firstStoneType = null;
        var stoneType = null;

        do {
          // this stone is of the same color, extend the connect
          xpos += i;
          ypos += j;
          if (xpos < 0 || xpos >= this.gameSizeTileX ||
              ypos < 0 || ypos >= this.gameSizeTileY) {
            break;
          }
          // after the first step, we determine which player has the longest
          // connect in this direction.
          if (firstStoneType === null) {
            firstStoneType = tiles[xpos][ypos].stoneType;
          }
          stoneType = tiles[xpos][ypos].stoneType;
          console.log('stonetype is ' + stoneType);
          if (stoneType === 1 || stoneType === 2) {
            stoneCounter++;
          }
          //console.log('i = ' + i + ', j = ' +  j + ', stonetype ' + stoneType + ' started with ' + firstStoneType);
        } while ((stoneType === 1 || stoneType === 2) && stoneType === firstStoneType);
        // stoneCounter is the length of the connect in this direction,
        // stoneType indicates the player (0, 1 or 2)

        console.log('stretch; ' + stoneCounter + ' for stonetype ' + firstStoneType);
        // if (stoneType == 1 && stoneCounter > maxPlayer1) {
        //   maxPlayer1 = stoneCounter;
        // } else if (stoneType == 2 && stoneCounter > maxPlayer2) {
        //   maxPlayer2 = stoneCounter;
        // }
        if (firstStoneType === 1 || firstStoneType === 2) {
          stretches[firstStoneType-1][i+1][j+1] = stoneCounter;
        }
      };
    };
    var maxStretches = [0, 0];
    for (var i = 0; i < stretches.length; i++) {
      var s = stretches[i]; // 3x3 array with stretches in all directions.
      var dirs = [[1, 0], [1, 1], [0, 1], [-1, 1]]; // orientations (two identical directions)
      for (var dir = 0; dir < dirs.length; dir++) {
        var d = dirs[dir];
        var stretch = stretches[i][d[0] + 1][d[1] + 1] + stretches[i][-d[0] + 1][-d[1] + 1];
        if (stretch > maxStretches[i]) {
          maxStretches[i] = stretch;
        }
      };
    };
    var m0 = maxStretches[0];
    var m1 = maxStretches[1];
    if (m0 < m1) {
      return [1, maxStretches] // player1 has shortest
    } else if (m0 > m1) {
      return [2, maxPlayer2]; // player2 has shortest
    } else {
      return [0, maxPlayer1]; // tie
    }
  }

  // computes the number of connect4's containing [baseX, baseY] in tiles, for player 1 and 2.
  // output: array with at position N - 1: playerN's number of connect4's.
  GameInstance.prototype.computeConnect4 = function(baseX, baseY) {
    var connect4CountPlayer1 = 0;
    var connect4CountPlayer2 = 0;
    var tiles = this.tiles;
    // compute horizontal, vertical, diagonal and anti-diagonal arrays in which [baseX,baseY] can form a new Connect 4

    // horizontal array horArray
    boundLeft =  Math.max(0, baseX-3);
    boundRight = Math.min(this.gameSizeTileX-1, baseX+3);
    horArray = new Array(boundRight-boundLeft+1);
    for (var i = boundLeft; i <= boundRight; i++){
      if (i === baseX) {
        horArray[i-boundLeft] = 100; // magic number 100 counts for any stone type.
      } else {
        horArray[i-boundLeft] = tiles[i][baseY].stoneType;
      }
    }
    // vertical array verArray
    boundTop = Math.max(0, baseY-3);
    boundBottom = Math.min(this.gameSizeTileY-1, baseY+3);
    verArray = new Array(boundBottom-boundTop+1);
    for (var j = boundTop; j <= boundBottom; j++){
      if (j === baseY) {
        verArray[j-boundTop] = 100; // magic number 100 counts for any stone type.
      } else {
        verArray[j-boundTop] =  tiles[baseX][j].stoneType;
      }
    }
    // diagonal array diagArray
    stepsDiagUp = Math.min(baseX,baseY,3);
    stepsDiagDown = Math.min(this.gameSizeTileX-1 - baseX, this.gameSizeTileY - 1 -baseY, 3);
    diagArray = new Array(stepsDiagUp + stepsDiagDown+1);
    for (var k = 0 ; k < diagArray.length; k++){
      if (k === stepsDiagUp) {
        diagArray[k] = 100; // magic number 100 counts for any stone type.
      } else {
        diagArray[k] = tiles[baseX-stepsDiagUp + k][baseY-stepsDiagUp + k].stoneType;
      }
    }  
    // anti-diagonal array antidiagArray
    stepsAntidiagUp = Math.min(baseY, this.gameSizeTileX - 1 - baseX, 3);
    stepsAntidiagDown = Math.min(baseX, this.gameSizeTileY - 1 - baseY, 3);
    antidiagArray = new Array(stepsAntidiagUp + stepsAntidiagDown + 1);
    console.log(antidiagArray + 'antidiag');
    for (var k = 0 ; k < antidiagArray.length; k++){
      if (k === stepsAntidiagUp) {
        antidiagArray[k] = 100; // magic number 100 counts for any stone type.
      } else {
        antidiagArray[k] = tiles[baseX + stepsAntidiagUp - k][baseY-stepsAntidiagUp + k].stoneType;
      }
    } 
    //antidiagArray = new Array();//(boundRight-boundLeft+1);
    console.log('hor : '+  horArray);
    console.log('ver : '+  verArray);
    console.log('diag : '+  diagArray);
    console.log('antidiag : '+  antidiagArray);
    
    var horizontals = this.computeConnect4InArray(horArray);
    var verticals = this.computeConnect4InArray(verArray);
    var diagonals = this.computeConnect4InArray(diagArray);
    var antidiagonals = this.computeConnect4InArray(antidiagArray);

    connect4CountPlayer1 += horizontals[0] + verticals[0] + diagonals[0] + antidiagonals[0];
    connect4CountPlayer2 += horizontals[1] + verticals[1] + diagonals[1] + antidiagonals[1];

    console.log('player 1: '+  connect4CountPlayer1 + ' extra Connect Fours.');
    console.log('player 2: '+  connect4CountPlayer2 + ' extra Connect Fours.');
    return [connect4CountPlayer1, connect4CountPlayer2];
  };

  // helper function
  // input: an array that represents a straight line of stones
  // output: array with at position N - 1: playerN's number of connect4's.
  GameInstance.prototype.computeConnect4InArray = function(stoneArray) {
    var connect4inArrayPlayer1 = 0;
    var connect4inArrayPlayer2 = 0;

    var previousStoneType = 0;
    var currentStoneType = 0;
    var stoneCounter = 0;
    for (var i = 0; i < stoneArray.length; i++){
      currentStoneType = stoneArray[i];
      if (currentStoneType !== 0) { // 1, 2 or 100
        if (stoneCounter === 0 || // we just reset, anything goes as a first stone.
            currentStoneType === previousStoneType || // the previous stone was the same, continue streak.
            currentStoneType === 100) { // the stone we wish to put down may have either color, continue streak.
          stoneCounter++;
        // if the previous stone was the stone we put down, the current streak depends on what came before it.
        } else if (previousStoneType == 100){
          // the stone before was the same as now; we have bridged the '100'-stone with the same color.
          if (i >= 2 && stoneArray[i - 2] === currentStoneType) {
            stoneCounter++;
          } else {
            // either the 100-stone was near the edge, or another color came before.
            stoneCounter = 2;
          }
        }
      } else {
        stoneCounter = 0;
      }
      if(stoneCounter === 4){
        if(currentStoneType === 1 || 
          (currentStoneType === 100 && previousStoneType === 1) ){
          connect4inArrayPlayer1 += 1; 
        } else if(currentStoneType === 2 ||
                (currentStoneType === 100 && previousStoneType === 2) ){
          connect4inArrayPlayer2 += 1; 
        }
        stoneCounter -=1;
      }
      previousStoneType = currentStoneType;
    }
    return [connect4inArrayPlayer1, connect4inArrayPlayer2];
  };
};

exports.GameInstance = GameInstance;