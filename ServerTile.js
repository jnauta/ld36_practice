var ServerTile = function(xpos, ypos, type) {
  this.xpos = xpos;
  this.ypos = ypos;
  // stoneType indicates the appearance of a stone:
  // 0 = non-played
  // 1 = player1
  // 2 = player2
  // negative = timeoutMarker
  this.stoneType = type;
  this.countdown = 0;
  this.decreaseCountdown = function() {
  	if(this.countdown > 0) {
  		--this.countdown;
  		if (this.countdown === 0) {
  			this.stoneType = 0;
  		}
  	}
  }

  return this;
}

exports.ServerTile = ServerTile;