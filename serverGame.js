var app = require('http').createServer(handler)
var io = require('socket.io')(app);
var fs = require('fs');
var Player = require('./player.js').Player;
var GameInstance = require('./GameInstance.js').GameInstance;
var ServerTile = require('./ServerTile.js').ServerTile;

app.listen(8000);

function handler(req, res) {
  console.log('handling ', __dirname);
  fs.readFile(__dirname + '/public/index.html',
    function(err, data) {
      if (err) {
        res.writeHead(500);
        return res.end('Error loading index.html');
      }

      res.writeHead(200);
      res.end(data);
    });
}

var games;
var idCounter = 0;
var playerIDs2Sockets;
var playerIDs2GameIDs;
var gameIDs2Games;

var nrOfChoicesCake = 5;

function init() {
  games = [];
  playerIDs2Sockets = new Map();
  playerIDs2GameIDs = new Map();
  gameIDs2Games = new Map();

  var objectKey = {
    foo: 'bar'
  };

  var x = new Map();
  x.set(1, 'keys are not converted to strings');
  x.set('1', 'this is stored separately than the previous value');
  x.set(objectKey, ['you use arbitrary objects as keys and values']);

  console.log(x);
  console.log(x.get(1));
  console.log(x.get('1'));
  console.log(x.get(objectKey));

  setEventHandlers();
};

var setEventHandlers = function() {
  console.log('setting connection handler');
  console.log(onSocketConnection);
  io.on("connection", onSocketConnection);
};

function onSocketConnection(client) {
  console.log(client.handshake.query.playerID);
  var playerID = client.handshake.query.playerID;
  var newPlayer = new Player(playerID);
  console.log(newPlayer);
  if (playerById(newPlayer.playerID)) {
    console.log(newPlayer.playerID + " already exists in player list");
    // and then what?
  } else {
    console.log("New player has connected with name " + newPlayer.playerID);
    playerIDs2Sockets.set(playerID, client);

    // list of players
    ListOfPlayers = [];
    playerIDs2Sockets.forEach(function(playerObj, playerID) {
       ListOfPlayers.push(playerID);

    });
    console.log('list of players: ' + ListOfPlayers);


    client.playerID = newPlayer.playerID;
    client.broadcast.emit("new player", {
      playerID: client.playerID
    });
    var i, existingPlayer;
    // for (i = 0; i < players.length; i++) {
    //     existingPlayer = players[i];
    //     client.emit("new player", {playerID: existingPlayer.playerID});
    // };
    playerIDs2Sockets.forEach(function(playerObj, playerID) {
      client.emit("new player", {
        playerID: playerID
      });
    });
    client.emit("update games", {
      gameIDs: Array.from(gameIDs2Games.keys())
    });

  }

  client.on("disconnect", onClientDisconnect);
  //client.on("new player", onNewPlayer);
  client.on("create game", onCreateGame);
  client.on("join game", onJoinGame);
  client.on("submit cake", onClientCake);
  client.on("player move", onPlayerMove);

};

function onClientDisconnect() {
  console.log("Player has disconnected: " + this.playerID);
  var removePlayer = playerById(this.playerID);
  if (!removePlayer) {
    console.log("Error: Player not found: " + this.playerID);
    return;
  };

  

  // players.splice(players.indexOf(removePlayer), 1);
  // if the disconnecting player was the only one in a game, remove it.
  var playerGameID = playerIDs2GameIDs.get(this.playerID);
  if (playerGameID) {
    gameIDs2Games.delete(playerGameID);
    playerIDs2GameIDs.delete(this.playerID);
    
    this.broadcast.emit("update games", {
      gameIDs: Array.from(gameIDs2Games.keys())
    });
  }

  // remove player from the Map playerIDs2Sockets
  playerIDs2Sockets.delete(this.playerID);

  this.broadcast.emit("remove player", {
    playerID: this.playerID
  });
};

function onCreateGame(data) {
  var newGame = new GameInstance(data.gameID, data.player1, null);
  gameIDs2Games.set(data.gameID, newGame);
  playerIDs2GameIDs.set(data.player1, data.gameID);
  io.emit("update games", {
    gameIDs: Array.from(gameIDs2Games.keys())
  });
};

// only called for joiner, not host
function onJoinGame(data) {
  var game2join = data.gameID;
  console.log('gameID = ' + game2join);
  var game = gameIDs2Games.get(game2join);
  console.log('game is ' + game);
  console.log('player ' + data.playerID + 'joining');
  game.player2 = data.playerID;
  playerIDs2GameIDs.set(data.playerID, data.gameID);
  // client1 created the game
  var client1 = playerIDs2Sockets.get(game.player1);
  // request both players' cakestyles for exchange
  client1.emit("cake please");
  // this is the player who joined
  this.emit("cake please");
};

function onClientCake(data) {
  console.log('cake received from ' + this.playerID);
  var sender = this.playerID;
  var otherClientID = null;
  var gameID = playerIDs2GameIDs.get(sender);
  var game = gameIDs2Games.get(gameID);
  var badCake = false;
  for (var i = data.cakeStyle.length - 1; i >= 0; i--) {
    if (data.cakeStyle[i] >= nrOfChoicesCake) {
      // hacking!
      badCake = true;
    }
  }
  if (badCake) {
    console.log('Hacking attempt on cakestyle?');
    return;
  }
  // assign the cakestyle to the right player
  console.log('this playerID: '+ this.playerID);
  if (this.playerID === game.player1) {
    game.cakeStyles[0] = data.cakeStyle;
    otherClientID = game.player2;
  } else if (this.playerID === game.player2) {
    game.cakeStyles[1] = data.cakeStyle;
    otherClientID = game.player1;
  } else {
    console.log('ERROR: received cake from player ' + this.playerID + ' for game with players ' + game.player1 + ' and ' + game.player2);
  }

  if (game.cakeStyles[0] && game.cakeStyles[1]) { // When both cakestyles have been received
    var sameCake = true;
    for (var i = 0; i < game.cakeStyles[0].length; ++i) {
      if (game.cakeStyles[0][i] != game.cakeStyles[1][i]){
        sameCake = false;
      } 
    }
    if(sameCake){
      game.cakeStyles[0][0] += 1;
      game.cakeStyles[0][0] = game.cakeStyles[0][0]  % nrOfChoicesCake;
      game.cakeStyles[1][0] += nrOfChoicesCake - 1;
      game.cakeStyles[1][0] = game.cakeStyles[1][0]  % nrOfChoicesCake;
      console.log('Same cake! Changing bottoms of both. Not telling them yet ;)');
    }
  

   

    // set the cakeStyle for tie-pie

    game.cakeStyles[2] = [nrOfChoicesCake, nrOfChoicesCake, nrOfChoicesCake];

    // send cakeStyles
    var player1Socket = playerIDs2Sockets.get(game.player1);
    player1Socket.emit("join game", {
      gameID: gameID,
      players: [game.player1, game.player2],
      stoneType: 1,
      cakeStyles: game.cakeStyles
    });
    var player2Socket = playerIDs2Sockets.get(game.player2);
    player2Socket.emit("join game", {
      gameID: gameID,
      players: [game.player1, game.player2],
      stoneType: 2,
      cakeStyles: game.cakeStyles
    });

    // for debugging, place some cake.
    var turnTemplate = {
      turnType: "regular",
      move1: {},
      move2: {},
      scores: [42, 1337]
    };
    var idxs1 = [
      [1, 2],
      [1, 3],
      [1, 4]
    ];
    var idxs2 = [
      [4, 2],
      [4, 3],
      [4, 4]
    ];
    //gameID, playerID (sender ,otherClientID), x, y
    for (var i = 0; i < idxs1.length; i++) {
      onPlayerMove({
        playerID: sender,
        gameID: gameID,
        xpos: idxs1[i][0],
        ypos: idxs1[i][1]
      });
      onPlayerMove({
        playerID: otherClientID,
        gameID: gameID,
        xpos: idxs2[i][0],
        ypos: idxs2[i][1]
      });
    };
  }
}
// data = {playerID, gameID, xpos, ypos}
onPlayerMove = function(data) {
  console.log("Player " + data.playerID + " moves: " + data.xpos + ', ' + data.ypos);
  var gameID = data.gameID;
  var game = gameIDs2Games.get(gameID);
  if (!game) {
    console.log("Tried to play a move, but game " + gameID + "not found.");
    return;
  };
  var movePlayer = data.playerID;
  var stoneType;
  if (game.player1 === movePlayer) {
    stoneType = 1;
    console.log('setting move 1, was ' + game.move1);
    game.submitMove({
      xpos: data.xpos,
      ypos: data.ypos,
      stoneType: stoneType
    });
  } else if (game.player2 === movePlayer) {
    stoneType = 2;
    console.log('setting move 2, was ' + game.move2);
    game.submitMove({
      xpos: data.xpos,
      ypos: data.ypos,
      stoneType: stoneType
    });
  } else {
    console.log("ERROR, player " + movePlayer + " played, but " + game.player1 + " and " + game.player2 + " are in this game!");
  }
  // If both players have now sent their moves, process both moves and emit result
  // turnResult depends on the type of result, see GameInstance.js.
  var turnResult = game.processMoves();
  console.log('scores; ' + game.scores[0] + ' - ' + game.scores[1]);

  //console.log('blah' + game.move1 + " and " + game.move2);
  if (turnResult) {
    var client1 = playerIDs2Sockets.get(game.player1);
    var client2 = playerIDs2Sockets.get(game.player2);
    client1.emit("player move", turnResult);
    client2.emit("player move", turnResult);
    var winner = game.decideWinner();
    console.log('winner: ' + winner);
    if (winner !== null) {
      client1.emit("player win", {
        winner: winner
      });
      client2.emit("player win", {
        winner: winner
      });
    }
  }
  // decide victory
  // process 

};

function playerById(playerID) {
  //return players.find(x=>x.playerID === this.playerID);
  return playerIDs2Sockets.get(playerID);
};

function gameById(gameID) {
  return games.find(x => x.gameID === data.gameID);
}

init();